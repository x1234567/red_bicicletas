var map = L.map('main_map').setView([6.235644, -75.579408], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([6.256876, -75.590685]).addTo(map)
    .bindPopup('Estadio Atanasio Girardot, Medellín.')
    .openPopup();

L.marker([6.235644, -75.579408]).addTo(map)
    .bindPopup('Mirador Cerro Nutibara y pueblito Paisa, Medellín =).')
    .openPopup();

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    succsess: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
            // .bindPopup('Medellín =)')
            // .openPopup();
        });
    }
})